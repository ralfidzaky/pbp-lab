import 'package:flutter/material.dart';
import 'package:lab_6/loginpage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Colors.red[400],
          fontFamily: 'Times'),
      home: LoginPage(),
    );
  }
}
