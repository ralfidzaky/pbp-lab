import 'package:flutter/material.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  final String lorem =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Donor Plasma'),
          backgroundColor: Colors.red[400],
          bottom: TabBar(
            tabs: [
              Tab(text: 'Donor'),
              Tab(text: 'Info UDD'),
              Tab(text: 'FAQ'),
              Tab(text: 'Artikel'),
            ],
          ),
        ),
        body: TabBarView(children: [
          Center(
            child: Text('Page 1'),
          ),
          Center(
            child: Text('Page 2'),
          ),
          Center(
            child: Text('Page 3'),
          ),
          Container(
            child: ListView(
              children: [
                createCard('Artikel 1'),
                createCard('Artikel 2'),
                createCard('Artikel 3'),
                createCard('Artikel 4'),
                createCard('Artikel 5'),
                createCard('Artikel 6'),
                createCard('Artikel 7'),
                createCard('Artikel 8'),
                createCard('Artikel 9'),
                createCard('Artikel 10'),
                createCard('Artikel 11'),
                createCard('Artikel 12'),
              ],
            ),
          ),
        ]),
      ),
    );
  }

  Container createCard(String text) {
    return Container(
      child: Card(
        elevation: 5,
        child: Row(
          children: [
            Container(
              width: 50,
              height: 50,
              margin: EdgeInsets.all(5),
              child: Image(
                image: AssetImage('images/dummies.png'),
              ),
            ),
            Text(text),
          ],
        ),
      ),
      height: 75,
    );
  }
}
