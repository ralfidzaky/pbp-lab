from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('add', add_friend, name='add_friend'),
]
