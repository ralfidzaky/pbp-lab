from lab_1.models import Friend
from django.forms import ModelForm

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']

    error_messages = {
        'required': 'Please complete the form'
    }