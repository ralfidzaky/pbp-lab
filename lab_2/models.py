from django.db import models

# Create your models here.
class Note(models.Model):
    toWho = models.CharField(max_length=30)
    fromWho = models.CharField(max_length=30)
    title = models.TextField()
    message = models.TextField()
