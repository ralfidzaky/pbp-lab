from django.shortcuts import redirect, render
from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm

def index(request):
    note = Note.objects.all().values()
    response = {'note': note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('/lab-4')

    context = {
        'form': form
    }
    return render(request, 'lab4_form.html', context)

def note_list(request):
    note = Note.objects.all().values()
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)